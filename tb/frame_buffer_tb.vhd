library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;
use std.env.finish;

entity frame_buffer_tb is
end frame_buffer_tb;

architecture sim of frame_buffer_tb is

    constant clk_hz     : integer := 50e6;
    constant clk_period : time    := 1 sec / clk_hz;

    signal clk           : std_logic                    := '0';
    signal data          : std_logic_vector(4 downto 0) := (others => '0');
    signal write_address : integer range 0 to 95999     := 0;
    signal read_address  : integer range 0 to 95999     := 0;
    signal we            : std_logic                    := '0';
    signal q             : std_logic_vector(4 downto 0);

begin

    clk <= not clk after clk_period / 2;

    DUT : entity work.frame_buffer(rtl)
        port map(
            clk           => clk,
            data          => data,
            write_address => write_address,
            read_address  => read_address,
            we            => we,
            q             => q
        );

    SEQUENCER_PROC : process
    begin
        wait for clk_period * 2;

        we <= '1';

        data          <= "11011";
        write_address <= 98;

        wait for clk_period * 2;

        data          <= "11101";
        write_address <= 99;

        wait for clk_period * 2;

        we <= '0';

        wait for clk_period * 5;

        read_address <= 97;

        wait for clk_period * 5;

        read_address <= 98;

        wait for clk_period * 5;

        read_address <= 99;

        wait for clk_period * 5;

        read_address <= 100;

        wait for clk_period * 10;

        finish;
    end process;

end architecture;
