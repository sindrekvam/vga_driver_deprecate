library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;
use std.env.finish;

entity character_to_fb_tb is
end character_to_fb_tb;

architecture sim of character_to_fb_tb is

    constant clk_hz     : integer := 50e6;
    constant clk_period : time    := 1 sec / clk_hz;

    signal clk             : std_logic := '1';
    signal write_enable    : std_logic := '0';
    signal valid_uart      : std_logic := '0';
    signal ascii_character : std_logic_vector(39 downto 0);
    signal addr            : integer range 0 to 95999;
    signal data            : std_logic_vector(4 downto 0);

begin

    clk <= not clk after clk_period / 2;

    DUT : entity work.character_to_fb(rtl)
        port map(
            CLK             => clk,
            WRITE_ENABLE    => write_enable,
            VALID_UART      => valid_uart,
            ASCII_CHARACTER => ascii_character,
            ADDR            => addr,
            DATA            => data
        );

    SEQUENCER_PROC : process
    begin
        wait for clk_period * 2;
        ascii_character <= "0000010001100011111110001100011000101110";

        wait for clk_period * 2;
        valid_uart <= '1';

        wait for clk_period * 1;
        valid_uart <= '0';

        wait for clk_period * 2;
        ascii_character <= (others => '0');

        wait for clk_period * 5;
        write_enable <= '1';

        wait for clk_period * 10;
        write_enable <= '0';

        wait for clk_period * 2;
        ascii_character <= "0000000010000101111110010010100011000010";

        wait for clk_period * 2;
        valid_uart <= '1';

        wait for clk_period * 1;
        valid_uart <= '0';

        wait for clk_period * 2;
        ascii_character <= (others => '0');

        wait for clk_period * 5;
        write_enable <= '1';

        wait for clk_period * 10;
        write_enable <= '0';

        wait for clk_period * 10;

        finish;
    end process;

end architecture;
