library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;
use std.env.finish;

entity uart_rx_tb is
end uart_rx_tb;

architecture sim of uart_rx_tb is

    constant clk_hz     : real := 50.0e6;
    constant clk_period : time    := 1 sec / clk_hz;

    constant baud_rate  : integer := 115200;
    constant clock_cycles_per_bit : integer := integer(clk_hz / real(baud_rate));
    constant clock_period_per_bit : time := clk_period * clock_cycles_per_bit;--1 sec / baud_rate;

    signal clk            : std_logic := '1';
    signal rst_n          : std_logic := '1';
    signal rx             : std_logic := '1';
    signal data           : std_logic_vector(7 downto 0);
    signal valid          : std_logic;
    signal stop_bit_error : std_logic;

begin

    clk <= not clk after clk_period / 2;

    DUT : entity work.uart_rx(rtl)
        port map(
            clk            => clk,
            rst_n          => rst_n,
            rx             => rx,
            data           => data,
            valid          => valid,
            stop_bit_error => stop_bit_error
        );

    SEQUENCER_PROC : process
        -------------------------------------------------
        -- Write
        -------------------------------------------------
        procedure write(
            constant DATA_VALUE : in std_logic_vector) is
        begin
            rst_n <= '1';
            wait for clock_period_per_bit;

            rx <= '0';
            wait for clock_period_per_bit;
            rx <= DATA_VALUE(7);
            wait for clock_period_per_bit;
            rx <= DATA_VALUE(6);
            wait for clock_period_per_bit;
            rx <= DATA_VALUE(5);
            wait for clock_period_per_bit;
            rx <= DATA_VALUE(4);
            wait for clock_period_per_bit;
            rx <= DATA_VALUE(3);
            wait for clock_period_per_bit;
            rx <= DATA_VALUE(2);
            wait for clock_period_per_bit;
            rx <= DATA_VALUE(1);
            wait for clock_period_per_bit;
            rx <= DATA_VALUE(0);
            wait for clock_period_per_bit;
            rx <= '1';
            wait for clock_period_per_bit;

        end procedure;

    begin

        write(x"4F");

        wait for clock_period_per_bit * 10;

        finish;
    end process;

end architecture;
