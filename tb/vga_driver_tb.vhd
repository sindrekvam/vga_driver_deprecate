library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

use std.textio.all;
use std.env.finish;

entity vga_driver_tb is
end vga_driver_tb;

architecture sim of vga_driver_tb is

    constant clk_hz     : integer   := 50e6;
    constant clk_period : time      := 1 sec / clk_hz;
    signal clk          : std_logic := '1';

    signal ram          : std_logic_vector(4 downto 0) := (others => '0');
    signal write_enable : std_logic;

    signal read_address : integer range 0 to 95999 := 0;

    signal h_sync : std_logic := '0';
    signal v_sync : std_logic := '0';

    signal r : std_logic_vector(3 downto 0) := (others => '0');
    signal g : std_logic_vector(3 downto 0) := (others => '0');
    signal b : std_logic_vector(3 downto 0) := (others => '0');

begin

    clk <= not clk after clk_period / 2;

    DUT : entity work.vga_driver(rtl)
        port map(
            clk          => clk,
            ram          => ram,
            write_enable => write_enable,
            read_address => read_address,
            h_sync       => h_sync,
            v_sync       => v_sync,
            r            => r,
            g            => b,
            b            => g
        );

    SEQUENCER_PROC : process
        variable seed1, seed2 : positive;

        impure function rand_slv(len : integer) return std_logic_vector is
            variable r   : real;
            variable slv : std_logic_vector(len - 1 downto 0);
        begin
            for i in slv'range loop
                uniform(seed1, seed2, r);
                slv(i) := '1' when r > 0.5 else '0';
            end loop;
            return slv;
        end function;
    begin
        wait for clk_period * 10;
        loop

            ram <= rand_slv(5);

            wait for clk_period * 5;

        end loop;

        --	assert false
        --	report "Replace this with your test cases"
        --	severity failure;

        finish;
    end process;

end architecture;
