library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;
use std.env.finish;

entity top_level_tb is
end top_level_tb;

architecture sim of top_level_tb is

    constant clk_hz     : integer := 50e6;
    constant clk_period : time    := 1 sec / clk_hz;

    signal clk             : std_logic                    := '0';
    signal arduino_io      : std_logic                    := '1';
    signal vertical_sync   : std_logic;
    signal horizontal_sync : std_logic;
    signal r               : std_logic_vector(3 downto 0);
    signal g               : std_logic_vector(3 downto 0);
    signal b               : std_logic_vector(3 downto 0);
    signal button          : std_logic_vector(1 downto 0) := "11";

begin

    clk <= not clk after clk_period / 2;

    DUT : entity work.top_level(rtl)
        port map(
            MAX10_CLK1_50 => clk,
            ARDUINO_IO0   => arduino_io,
            VGA_VS        => vertical_sync,
            VGA_HS        => horizontal_sync,
            VGA_R         => r,
            VGA_G         => g,
            VGA_B         => b,
            KEY           => button
        );

    SEQUENCER_PROC : process
    begin
        wait for clk_period * 10;

        button <= "00";

        wait for clk_period;
        button     <= "11";
        wait for clk_period;
        arduino_io <= '0';
        wait for clk_period * 433 * 1;
        arduino_io <= '1';
        wait for clk_period * 433 * 1;
        arduino_io <= '0';
        wait for clk_period * 433 * 5;
        arduino_io <= '1';
        wait for clk_period * 1e4;

        finish;
    end process;

end architecture;
