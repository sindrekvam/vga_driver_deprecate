library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use std.textio.all;
use std.env.finish;

entity ascii_table_tb is
end ascii_table_tb;

architecture sim of ascii_table_tb is

    constant clk_hz     : integer := 50e6;
    constant clk_period : time    := 1 sec / clk_hz;

    signal clk     : std_logic := '1';
    signal address : std_logic_vector(7 downto 0);
    signal q       : std_logic_vector(39 downto 0);

begin

    clk <= not clk after clk_period / 2;

    DUT : entity work.ascii_table(SYN)
        port map(
            address => address,
            clock   => clk,
            q       => q
        );

    SEQUENCER_PROC : process
    begin
        wait for clk_period * 2;

        address <= "01000001";

        wait for clk_period * 10;

        finish;
    end process;

end architecture;
