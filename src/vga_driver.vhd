library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vga_driver is
    port(
        clk          : in  std_logic;
        ram          : in  std_logic_vector(4 downto 0);
        write_enable : out std_logic;
        read_address : out integer range 0 to 95999     := 0;
        h_sync       : out std_logic                    := '0';
        v_sync       : out std_logic                    := '0';
        r            : out std_logic_vector(3 downto 0) := (others => '0');
        g            : out std_logic_vector(3 downto 0) := (others => '0');
        b            : out std_logic_vector(3 downto 0) := (others => '0')
    );
end entity vga_driver;

architecture RTL of vga_driver is
    constant HD  : integer := 800;      -- Horizontal display (800)
    constant HFP : integer := 56;       -- Horizontal front porch (56)
    constant HSP : integer := 120;      -- Horizontal sync pulse (retrace) (120)
    constant HBP : integer := 64;       -- Horizontal back porch (64)

    constant VD  : integer := 600;      -- Vertical display (600)
    constant VFP : integer := 37;       -- Vertical front porch (37)
    constant VSP : integer := 6;        -- Vertical sync pulse (retrace) (6)
    constant VBP : integer := 23;       -- Vertical back porch (23)

    signal vPos : integer := 0;
    signal hPos : integer := 0;

    signal draw : std_logic;
begin
    process(clk) is
        variable v_r : std_logic_vector(3 downto 0);
        variable v_g : std_logic_vector(3 downto 0);
        variable v_b : std_logic_vector(3 downto 0);
    begin
        if rising_edge(clk) then

            v_r := (others => '0');
            v_g := (others => '0');
            v_b := (others => '0');

            if hPos < HD + HFP + HSP + HBP then -- if hpos is less than the entire screen
                hPos <= hPos + 1;
                if hPos > HD + HFP and hPos < HD + HFP + HSP then -- if hpos is within the horizontal sync pulse
                    h_sync <= '0';
                else
                    h_sync <= '1';
                end if;
            else
                hPos <= 0;
                if vPos < VD + VFP + VSP + VBP then -- if vpos is less than the entire screen
                    vPos <= vPos + 1;
                    if vPos > VD + VFP and vPos < VD + VFP + VSP then -- if vpos is within the vertical sync pulse
                        v_sync <= '0';
                    else
                        v_sync <= '1';
                    end if;
                else
                    vPos <= 0;
                end if;
            end if;

            write_enable <= '0';

            if hPos < HD and vPos < VD then -- if the counter is in the visible screen
                -- Display here:

                v_r := "0000";
                v_g := "0000";
                v_b := "0000";

                if hPos mod 5 = 4 and read_address < 95999 then
                    read_address <= read_address + 1;
                end if;

                draw <= ram(hPos mod 5);

                if draw = '1' then      -- make colors changeable by software?
                    v_r := "1111";
                    v_g := "1111";
                    v_b := "1111";
                end if;
            else                        -- if the counter is out of the visible screen
                v_r          := "0000";
                v_g          := "0000";
                v_b          := "0000";
                write_enable <= '1';
                if vPos > VD then
                    read_address <= 0;
                end if;
            end if;
        end if;

        r <= v_r;
        g <= v_g;
        b <= v_b;

    end process;

end architecture RTL;
