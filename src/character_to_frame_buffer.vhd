library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity character_to_fb is
    port(
        CLK             : in  std_logic;
        WRITE_ENABLE    : in  std_logic;
        VALID_UART      : in  std_logic;
        ASCII_CHARACTER : in  std_logic_vector(39 downto 0);
        ADDR            : out integer range 0 to 95999;
        DATA            : out std_logic_vector(4 downto 0)
    );
end character_to_fb;

architecture rtl of character_to_fb is
    signal valid_uart_p1     : std_logic := '0';
    signal valid_uart_p2     : std_logic := '0';
    signal valid_uart_p3     : std_logic := '0';
    signal char_line_counter : integer   := 0;

    signal ascii_character_reg : std_logic_vector(39 downto 0); -- := (others => '0');
    signal char_num_p1         : integer := 0;
    signal char_num            : integer := 0;

    constant C_LINES_PER_CHAR : integer := 8;
    constant C_CHARS_PER_LINE : integer := 160; -- screen width / char width
begin

    character_to_fb_proc : process(CLK)
        variable vpos      : integer := 0;
        variable hpos      : integer := 0;
        variable char_line : integer := 0;

        variable hpos_end : integer := 159;
        constant vpos_end : integer := 600;

    begin
        if rising_edge(CLK) then

            valid_uart_p1 <= VALID_UART;
            valid_uart_p2 <= valid_uart_p1;
            valid_uart_p3 <= valid_uart_p2;

            if valid_uart_p2 = '1' and valid_uart_p3 = '0' then -- do it this way to delay it one clock cycle so that the ascii table has time to spit out the character

                ascii_character_reg <= ASCII_CHARACTER;
                char_num            <= char_num + 1;

            elsif (WRITE_ENABLE = '1' and (char_num /= char_num_p1)) or (char_line_counter /= 0) then

                -- CHARACTER WILL BE UPSIDE DOWN (CHANGE THIS)
                DATA <= ascii_character_reg(((char_line_counter * 5) + 4) downto char_line_counter * 5); --ascii_character_reg(((char_line_counter * 5) + 4) downto char_line_counter * 5); 
                ADDR <= (160 * char_line_counter) + char_num_p1;

                if char_line_counter >= 7 then
                    char_line_counter <= 0;
                    char_num_p1       <= char_num;
                else
                    char_line_counter <= char_line_counter + 1;
                end if;

                if hpos < hpos_end then
                    hpos := hpos + 1;

                else

                    if vpos < 600 then
                        vpos := vpos + 1;
                    else
                        vpos := 0;
                    end if;

                    hpos     := vpos * C_CHARS_PER_LINE * C_LINES_PER_CHAR;
                    hpos_end := hpos + (C_CHARS_PER_LINE - 1);
                end if;                 -- hpos equals hpos_end and char_num is not equals to char_num_p1

            end if;                     -- write_enable
        end if;                         -- rising_edge(clk)
    end process character_to_fb_proc;

end architecture;
