library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity top_level is
    port(
        MAX10_CLK1_50 : in  std_logic;
        ARDUINO_IO0   : in  std_logic;
        VGA_VS        : out std_logic;
        VGA_HS        : out std_logic;
        VGA_R         : out std_logic_vector(3 downto 0);
        VGA_G         : out std_logic_vector(3 downto 0);
        VGA_B         : out std_logic_vector(3 downto 0);
        KEY           : in  std_logic_vector(1 downto 0)
    );
end entity top_level;

architecture RTL of top_level is
    signal clk : std_logic;

    -- Uart module
    signal uart_data  : std_logic_vector(7 downto 0);
    signal uart_valid : std_logic;

    -- ascii table
    signal ascii_character : std_logic_vector(39 downto 0);

    -- character to frame buffer
    signal frame_buffer_write : std_logic_vector(4 downto 0);

    -- frame buffer
    signal read_address      : integer range 0 to 95999;
    signal write_address     : integer range 0 to 95999;
    signal frame_buffer_read : std_logic_vector(4 downto 0);

    -- vga-driver
    signal write_enable : std_logic;

begin

    clock : entity work.clock
        port map(
            areset => '0',
            inclk0 => MAX10_CLK1_50,
            c0     => clk,
            locked => open
        );

    uart_rx : entity work.uart_rx
        port map(
            clk            => clk,
            rst_n          => KEY(0),
            rx             => ARDUINO_IO0,
            data           => uart_data,
            valid          => uart_valid,
            stop_bit_error => open
        );

    ascii_table : entity work.ascii_table
        port map(
            address => uart_data,
            clock   => clk,
            q       => ascii_character
        );

    character_to_frame_buffer : entity work.character_to_fb
        port map(
            clk             => clk,
            write_enable    => write_enable,
            valid_uart      => uart_valid,
            ascii_character => ascii_character,
            addr            => write_address,
            data            => frame_buffer_write
        );

    frame_buffer : entity work.frame_buffer
        port map(
            clk           => clk,
            data          => frame_buffer_write,
            write_address => write_address,
            read_address  => read_address,
            we            => write_enable,
            q             => frame_buffer_read
        );

    internal_vga_driver : entity work.vga_driver
        port map(
            clk          => clk,
            ram          => frame_buffer_read,
            write_enable => write_enable,
            read_address => read_address,
            h_sync       => VGA_HS,
            v_sync       => VGA_VS,
            r            => VGA_R,
            g            => VGA_G,
            b            => VGA_B
        );

end architecture rtl;
