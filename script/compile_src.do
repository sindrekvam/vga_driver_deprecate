#-----------------------------------------------------------------------
# Compile source files
#-----------------------------------------------------------------------
quietly set lib_name "work"

echo "\n-----------------------------------------------------------------------"
echo "Compiling source files"
echo "-----------------------------------------------------------------------"

echo "\nCompiling clock"
eval vcom -reportprogress 300 -work work ../src/clock.vhd

echo "\nCompiling uart_rx"
eval vcom -reportprogress 300 -work work ../src/uart_rx.vhd

echo "\nCompiling ascii_table"
eval vcom -reportprogress 300 -work work ../src/ascii_table.vhd

echo "\nCompiling character_to_frame_buffer"
eval vcom -reportprogress 300 -work work ../src/character_to_frame_buffer.vhd

echo "\nCompiling frame_buffer"
eval vcom -reportprogress 300 -work work ../src/frame_buffer.vhd

echo "\nCompiling vga_driver"
eval vcom -reportprogress 300 -work work ../src/vga_driver.vhd

echo "\nCompiling top_level"
eval vcom -reportprogress 300 -work work ../src/top_level.vhd