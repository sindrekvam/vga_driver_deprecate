#-----------------------------------------------------------------------
# Compile testbench files
#-----------------------------------------------------------------------
quietly set lib_name "work"

echo "\n-----------------------------------------------------------------------"
echo "Compiling test benches"
echo "-----------------------------------------------------------------------"

echo "\nCompiling uart_rx_tb"
eval vcom -reportprogress 300 -work work ../tb/uart_rx_tb.vhd

echo "\nCompiling ascii_table_tb"
eval vcom -reportprogress 300 -work work ../tb/ascii_table_tb.vhd

echo "\nCompiling character_to_frame_buffer_tb"
eval vcom -reportprogress 300 -work work ../tb/character_to_frame_buffer_tb.vhd

echo "\nCompiling frame_buffer_tb"
eval vcom -reportprogress 300 -work work ../tb/frame_buffer_tb.vhd

echo "\nCompiling vga_driver_tb"
eval vcom -reportprogress 300 -work work ../tb/vga_driver_tb.vhd

echo "\nCompiling top_level_tb"
eval vcom -reportprogress 300 -work work ../tb/top_level_tb.vhd