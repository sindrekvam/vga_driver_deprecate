onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider uart
add wave -noupdate -color {Orange Red} /top_level_tb/DUT/uart_rx/rst_n
add wave -noupdate -color {Orange Red} /top_level_tb/DUT/uart_rx/rx
add wave -noupdate -color {Orange Red} /top_level_tb/DUT/uart_rx/data
add wave -noupdate -color {Orange Red} /top_level_tb/DUT/uart_rx/valid
add wave -noupdate -color {Orange Red} /top_level_tb/DUT/uart_rx/stop_bit_error
add wave -noupdate -color {Orange Red} /top_level_tb/DUT/uart_rx/state
add wave -noupdate -divider ascii_table
add wave -noupdate /top_level_tb/DUT/ascii_table/address
add wave -noupdate /top_level_tb/DUT/ascii_table/q
add wave -noupdate -divider character_to_fb
add wave -noupdate -color {Medium Violet Red} /top_level_tb/DUT/character_to_frame_buffer/WRITE_ENABLE
add wave -noupdate -color {Medium Violet Red} /top_level_tb/DUT/character_to_frame_buffer/VALID_UART
add wave -noupdate -color {Medium Violet Red} /top_level_tb/DUT/character_to_frame_buffer/ASCII_CHARACTER
add wave -noupdate -color {Medium Violet Red} /top_level_tb/DUT/character_to_frame_buffer/ADDR
add wave -noupdate -color {Medium Violet Red} /top_level_tb/DUT/character_to_frame_buffer/DATA
add wave -noupdate -color {Medium Violet Red} /top_level_tb/DUT/character_to_frame_buffer/valid_uart_p1
add wave -noupdate /top_level_tb/DUT/character_to_frame_buffer/valid_uart_p2
add wave -noupdate -color {Medium Violet Red} /top_level_tb/DUT/character_to_frame_buffer/ascii_character_reg
add wave -noupdate -divider frame_buffer
add wave -noupdate /top_level_tb/DUT/frame_buffer/data
add wave -noupdate /top_level_tb/DUT/frame_buffer/write_address
add wave -noupdate /top_level_tb/DUT/frame_buffer/read_address
add wave -noupdate /top_level_tb/DUT/frame_buffer/we
add wave -noupdate /top_level_tb/DUT/frame_buffer/q
add wave -noupdate -divider vga_driver
add wave -noupdate -color White /top_level_tb/DUT/MAX10_CLK1_50
add wave -noupdate -color White /top_level_tb/DUT/VGA_VS
add wave -noupdate -color White /top_level_tb/DUT/VGA_HS
add wave -noupdate -color White /top_level_tb/DUT/VGA_R
add wave -noupdate -color White /top_level_tb/DUT/VGA_G
add wave -noupdate -color White /top_level_tb/DUT/VGA_B
add wave -noupdate -color White /top_level_tb/DUT/KEY
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {82716112 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 357
configure wave -valuecolwidth 105
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {82642403 ps} {82909887 ps}
